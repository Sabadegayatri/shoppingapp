package com.example.contoller;

import com.example.ShoppingCartAssignmemnt.DiscountSlab;
import com.example.ShoppingCartAssignmemnt.Product;
import com.example.ShoppingCartAssignmemnt.ShoppingCartItem;

import java.util.List;

public class ShoppingCalculationImpl implements ShoppingCalculation {

    private List<Product> products;
    private  List<ShoppingCartItem> shoppingCartItems;
    private List<DiscountSlab> ds;
    private boolean isFlatdiscountapplicable;

    public ShoppingCalculationImpl(List<Product> p, List<ShoppingCartItem> sp,List<DiscountSlab> ds){
        this.products = p;
        this.shoppingCartItems = sp;
        this.ds = ds;
    }
    @Override
    public double calculateTotal() {
        double disTotal = 0.0;
        double total = 0.0;
        double prodDic = 0;
        int pId;
        System.out.println("Final bill: ");
        System.out.println("Item id"+" "+"Unit price: "+" "+"Quantity"+ " " +"Discount: ");
        for (ShoppingCartItem sp : shoppingCartItems) {
            double unitprice = sp.getUnitPrice();
            int quantity = sp.getQuantity();
            total  = ((unitprice * quantity) + total);
            prodDic = getProductDiscount(sp.getItemCategoryID());

            System.out.println(sp.getIteamId()+"  "+sp.getUnitPrice()+"  "+sp.getQuantity()+ "  "+prodDic);
            disTotal = total - (total * (prodDic / 100));
        }

        double flattotal =   calculateFlatDiscount(total);
        if(!isFlatdiscountapplicable){
            System.out.println("Total bill with discount: "+disTotal);
            return disTotal;
        }else{
            System.out.println("Total bill with flat discount: "+flattotal);
        }
        return flattotal;

    }

    @Override
    public double calculateFlatDiscount(double total) {
        double dispercentage = 0;
        double discountPrice = 0;
       for(DiscountSlab dslist : ds){
           if((dslist.getMinRange() <= total) && (dslist.getMaxRange() >= total)){
               dispercentage = dslist.getDispercentage();
               isFlatdiscountapplicable = true;
           }
           discountPrice =  total - (total * (dispercentage / 100));

       }
        return discountPrice;
    }

    public double  getProductDiscount( int pId){

        double dicountpercentage = 0.0;
        for (Product pr : products) {
            if(pId == pr.getpId()){
                dicountpercentage = pr.getDiscper();
            }
        }
        return dicountpercentage;
    }
}
