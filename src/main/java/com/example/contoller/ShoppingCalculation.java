package com.example.contoller;

public interface ShoppingCalculation {
    public double calculateTotal();
    public double calculateFlatDiscount(double total);
}
