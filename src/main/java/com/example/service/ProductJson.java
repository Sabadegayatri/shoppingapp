package com.example.service;

import com.example.ShoppingCartAssignmemnt.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductJson {

    JSONParser parser = new JSONParser();
    ObjectMapper mapper = new ObjectMapper();

    public List<Product> readProductJson() {
        List<Product> list = new ArrayList<>();
        Product product ;
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/Category.json"));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray c = (JSONArray) jsonObject.get("Category");

            for (int i = 0; i < c.size(); i++) {
                product = new Product();
                JSONObject jsonObjectRow = (JSONObject) c.get(i);

                String id = (String) jsonObjectRow.get("pId");
                product.setpId(Integer.parseInt(id));
                product.setPname((String) jsonObjectRow.get("pname"));
                String discper = (String) jsonObjectRow.get("discper");
                product.setDiscper(Integer.parseInt(discper));
                list.add(product);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }
}
