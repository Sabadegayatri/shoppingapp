package com.example.service;

import com.example.ShoppingCartAssignmemnt.ShoppingCartItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCartJson {

    JSONParser parser = new JSONParser();
    ObjectMapper mapper = new ObjectMapper();

    public List<ShoppingCartItem> readShoppingCartJson(){
        List<ShoppingCartItem> list = new ArrayList<ShoppingCartItem>();
        ShoppingCartItem spc ;
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/ShoppingCartIteam.json"));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray c = (JSONArray) jsonObject.get("ShoppingCart");

            for (int i = 0; i < c.size(); i++) {
                spc = new ShoppingCartItem();
                JSONObject jsonObjectRow = (JSONObject) c.get(i);

                String itemID = (String) jsonObjectRow.get("itemID");
                spc.setIteamId(Integer.parseInt(itemID));
                String itemCategoryID = (String) jsonObjectRow.get("itemCategoryID");
                spc.setItemCategoryID(Integer.parseInt(itemCategoryID));
                spc.setIteamName((String) jsonObjectRow.get("itemName"));
                String unitPrice = (String) jsonObjectRow.get("unitPrice");
                spc.setUnitPrice(Integer.parseInt(unitPrice));
                String quantity = (String) jsonObjectRow.get("quantity");
                spc.setQuantity(Integer.parseInt(quantity));
                list.add(spc);


            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }
}
