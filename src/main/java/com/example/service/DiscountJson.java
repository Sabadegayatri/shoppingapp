package com.example.service;

import com.example.ShoppingCartAssignmemnt.DiscountSlab;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DiscountJson {

    JSONParser parser = new JSONParser();
    ObjectMapper mapper = new ObjectMapper();

    public List<DiscountSlab> readDiscountJson() {
        List<DiscountSlab> list = new ArrayList<>();
        DiscountSlab ds ;
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/DiscountSlab.json"));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray c = (JSONArray) jsonObject.get("Slab");

            for (int i = 0; i < c.size(); i++) {
                ds = new DiscountSlab();
                JSONObject jsonObjectRow = (JSONObject) c.get(i);
                String min = (String) jsonObjectRow.get("RangeMin");
                ds.setMinRange(Integer.parseInt(min));
                String max = (String) jsonObjectRow.get("RangeMax");
                ds.setMaxRange(Integer.parseInt(max));
                String discper = (String) jsonObjectRow.get("discPerc");
                ds.setDispercentage(Integer.parseInt(discper));
                list.add(ds);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }

}
