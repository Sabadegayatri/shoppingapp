package com.example.ShoppingCartAssignmemnt;
import com.example.contoller.Products;
import com.example.contoller.ShoppingCalculationImpl;
import com.example.service.DiscountJson;
import com.example.service.ProductJson;
import com.example.service.ShoppingCartJson;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.List;


@SpringBootApplication
public class ShoppingCartAssignmemntApplication {

	public static void main(String[] args) {
        SpringApplication.run(ShoppingCartAssignmemntApplication.class, args);
        ShoppingCartAssignmemntApplication sp = new ShoppingCartAssignmemntApplication();

        ProductJson pj = new ProductJson();
        List<Product> list = pj.readProductJson();

        DiscountJson dj = new DiscountJson();
        List<DiscountSlab> disList = dj.readDiscountJson();

        ShoppingCartJson scj = new ShoppingCartJson();
        List<ShoppingCartItem> spcitem = scj.readShoppingCartJson();

        ShoppingCalculationImpl scim = new ShoppingCalculationImpl(list,spcitem,disList);


        System.out.println("List of Shopping Catagories  "+ "Catagories Id "+" && " +"Discount applied: ");
        for(Product p : list) {
            System.out.print(p.getPname()+ "" + p.getpId()+"           "+p.getDiscper()+"%");
            System.out.println("");
        }
        System.out.println("List of items in Shopping Cart:");
        System.out.println("Item: "+" "+"Quantity: "+"Price: ");
        for(ShoppingCartItem spt : spcitem) {
            System.out.print(spt.getIteamName()+"  "+spt.getQuantity()+" "+spt.getUnitPrice());
            System.out.println("");
        }
       System.out.println("Flat discount on shopping: ");
       System.out.println("MinAmount"+"  "+"MaxAmount"+"  "+"flatdiscount: ");
       for(DiscountSlab d : disList){
           System.out.print(d.getMinRange()+" "+d.getMaxRange()+"  "+d.getDispercentage()+"%");
           System.out.println("");
       }
        double total = scim.calculateTotal();

    }


}





