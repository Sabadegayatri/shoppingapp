package com.example.ShoppingCartAssignmemnt;

public class Product {
    private int pId;
    private String pname;
    private int discper;

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public int getDiscper() {
        return discper;
    }

    public void setDiscper(int discper) {
        this.discper = discper;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("***** shoppingCart Details *****\n");
        sb.append("ID="+getpId()+"\n");
        sb.append("Name="+getPname()+"\n");

        sb.append("Discounyt="+getDiscper()+"\n");


        return sb.toString();
    }
}



