package com.example.ShoppingCartAssignmemnt;

public class ShoppingCartItem {
   private int iteamId;
   private int itemCategoryID;
    private String iteamName;
    private double unitPrice;
    private int quantity;

    public int getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(int itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    public int getIteamId() {
        return iteamId;
    }

    public void setIteamId(int iteamId) {
        this.iteamId = iteamId;
    }

    public String getIteamName() {
        return iteamName;
    }

    public void setIteamName(String iteamName) {
        this.iteamName = iteamName;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double totalPurchasePrice(){
        double  total = this.unitPrice * this.quantity;
        return total;
    }

}

