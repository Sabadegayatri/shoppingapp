package com.example.ShoppingCartAssignmemnt;

public class DiscountSlab {
    private int minRange;
    private int maxRange;
    private int dispercentage;

    public int getMinRange() {
        return minRange;
    }

    public void setMinRange(int minRange) {
        this.minRange = minRange;
    }
    public int getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public double getDispercentage() {
        return dispercentage;
    }

    public void setDispercentage(int dispercentage) {
        this.dispercentage = dispercentage;
    }
}

